import { Component } from '@angular/core';
import {Card, CardDeck} from "../lib/CardDeck";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Black Jack';
  cardDeck: CardDeck = new CardDeck();
  hand: Card[] = [];

  constructor() {
    this.startNewGame();
  }

  startNewGame() {
    this.cardDeck = new CardDeck();
    this.hand = this.cardDeck.getCards(2);
  }

  giveAnotherCard() {
    const card = this.cardDeck.getCard();
    this.hand.push(card);
  }

  getTotalScore() {
    let sum = 0;
    this.hand.forEach(card => {
      sum += card.getScore();
    });
    return sum;
  }
}
