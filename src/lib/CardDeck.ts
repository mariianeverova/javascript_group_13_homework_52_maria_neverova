const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const suits = ['diams', 'hearts', 'clubs', 'spades'];

export class Card {
  rank: string;
  suit: string;

  constructor(rank: string, suit: string) {
    this.rank = rank;
    this.suit = suit;
  }

  getScore(): number {
    if (this.rank === 'A') {
      return 11;
    }

    if (this.rank === 'J' || this.rank === 'Q' || this.rank === 'K') {
      return 10;
    }

    return parseInt(this.rank);
  }
}

export class CardDeck{
  cards: Card[] = [];

  constructor() {
    for (const rank of ranks) {
      for (const suit of suits) {
        const card = new Card(rank, suit);
        this.cards.push(card);
      }
    }
  }

  getCard(): Card {
    const randomIndex = Math.floor(Math.random() * this.cards.length);
    const result = this.cards.splice(randomIndex, 1);
    return result[0];
  }

  getCards(howMany: number): Card[] {
    const usedCards: Card[] = [];

    for (let i = 0; i < howMany; i++) {
      usedCards.push(this.getCard());
    }
    return usedCards;
  }
}
